(defpackage SISA/tests/main
  (:use :cl
        :SISA
        :rove))
(in-package :SISA/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :SISA)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
